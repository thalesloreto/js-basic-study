function Pessoa(nome) {
  this.nome = nome;
  this.falar = function() {
    console.log(`Nome: ${this.nome}`);
  };
}

const p1 = new Pessoa("Thales");
p1.falar();
console.log(p1.nome);
