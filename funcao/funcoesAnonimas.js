const soma = function(x = 0, y = 0) {
  return x + y;
};

const imprimeResultado = function(x = 0, y = 0, operador = soma) {
  console.log(operador(x, y));
};

imprimeResultado(3, 4);
imprimeResultado(3, 4, soma);

imprimeResultado(3, 4, function(x = 0, y = 0) {
  return x - y;
});

imprimeResultado(3, 4, (x, y) => x * y);

const pessoa = {
  falar: function() {
    console.log("Olá");
  }
};

pessoa.falar();
