const fabricantes = ["Mercedes", "Audi", "Ferrari"];

function imprimir(conteudo, indice) {
  console.log(`${indice + 1}. ${conteudo}`);
}

fabricantes.forEach(imprimir);

fabricantes.forEach(function(a) {
  // a == conteudo
  console.log(a);
});

fabricantes.forEach(fabricante => console.log(fabricante));
