function criarProduto(nome, preco) {
  return {
    nome: nome,
    preco: preco,
    desconto: 0.1
  };
}

console.log(criarProduto("Cadeira Gamer", 1549.5));
console.log(criarProduto("Xiaomi mi8", 1900));
