//forma literal
function fun1() {}

// armazenar em uma variavel
const fun2 = function() {};

//armazenar em um array
const array = [
  function(a, b) {
    return a + b;
  },
  fun1,
  fun2
];
console.log(array[0](2, 3));

//armazenar num objeto
const obj = {
  falar: function() {
    return "Opa";
  }
};

console.log(obj.falar());

//passando fun como param pra outa fun
function run(fun) {
  fun();
}

run(function() {
  console.log("Executando...");
});

// uma função pode retornar / conter uma função

function soma(a, b) {
  return function(c) {
    console.log(a + b + c);
  };
}

soma(2, 3)(5);
