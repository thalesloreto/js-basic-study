const person = {
  say: "Bom dia!",
  speak() {
    console.log(this.say);
  }
};

person.speak(); // ok
const falar = person.speak;
falar(); // conflito

const falarPessoa = person.speak.bind(person); // correto
falarPessoa();
