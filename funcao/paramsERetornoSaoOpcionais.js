function area(altura, largura) {
  const area = altura * largura;
  if (area > 20) {
    console.log(`Área maior que permitido: ${area}m².`);
  } else {
    return area;
  }
}

console.log(area(2, 2));
console.log(area(2));
console.log(area());
console.log(area(2, 5, 4));
console.log(area(5, 5));
