function getRandInteger(min = 0, max = 10) {
    const value = Math.random() * (++max - min) + min;
    return Math.floor(value);
}

let option = -1;

do {
    option = getRandInteger(-1, 10);
    console.log(`Número sorteado: ${option}`);
} while(option != -1);