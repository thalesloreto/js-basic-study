Number.prototype.entre = function (inicio, fim) {
    return this >= inicio && this <= fim;
}

// Number.prototype.entre = (inicio, fim) => this >= inicio && this <= fim; // função Arrow.

const imprimirNota = function (nota) {
    if (nota.entre(9, 10)) {
        console.log('Menção Honrosa!');
    }
    else if (nota.entre(6, 8.99)) {
        console.log('Aprovado');
    }
    else if (nota.entre(2.5, 5.99)) {
        console.log('Exame');
    }
    else if (nota.entre(0, 2.4)) {
        console.log('Reprovado');
    }
    else {
        console.log('Nota inválida');
    }
}

imprimirNota(10);
imprimirNota(7.3);
imprimirNota(5.3);
imprimirNota(2);
imprimirNota(11);