function soBoaNota(nota) {
    if (nota >= 7) {
        console.log('Nota Boa:', nota);
    } else {
        console.log('Nota ruim:', nota);
    }
}

soBoaNota(6.9);

function seForVerdadeEuFalo(valor) {
    if(valor) {
        console.log('É verdade...', valor);
    }
}

seForVerdadeEuFalo(-1);