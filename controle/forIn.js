const notas = [5.6, 3.5, 10, 8.3, 7.3];

for (let i in notas) {
    console.log(i, notas[i]);
}

const pessoa = {
    nome: 'Thales',
    idade: 19,
    peso: 61.8
}

for (let atributo in pessoa) {
    console.log(`${atributo} = ${pessoa[atributo]}`);
}