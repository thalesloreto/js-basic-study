function Aula(nome, videoID) {
  this.nome = nome;
  this.videoID = videoID;
}

const aula1 = new Aula("Aula 01", 001);
const aula2 = new Aula("Aula 02", 002);
console.log(aula1, aula2);

// simulando o New
function novo(func, ...params) {
  const obj = {};
  obj.__proto__ = func.prototype;
  func.apply(obj, params);
  return obj;
}

const aula3 = novo(Aula, "Aula 03", 003);
const aula4 = novo(Aula, "Aula 04", 004);
console.log(aula3, aula4);