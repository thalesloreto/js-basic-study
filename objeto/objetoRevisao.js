const produto = new Object();
produto.nome = "Cadeira";
produto.preco = 1200;

console.log(produto);

delete produto.preco;
console.log(produto);

const carro = {
  modelo: "A4",
  preco: 89000,
  proprietario: {
    nome: "Thales",
    idade: 19,

    endereco: {
      rua: "Av. Reserva do Japy",
      numero: 385,
      bairro: "Gramadão"
    }
  },
  condutores: [
    {
      nome: "Cindy",
      idade: 19
    },
    {
      nome: "Marcio",
      idade: 46
    }
  ],
  calcularValorSeguro: function() {
    // ...
  }
};

carro.proprietario.endereco.numero = 1000;
console.log(carro);
