const pessoa = {
  nome: "Valdir",
  idade: 58,
  peso: 82.5
};

console.log(Object.keys(pessoa)); // pega as chaves do obj
console.log(Object.values(pessoa)); // pega os valores
console.log(Object.entries(pessoa)); // tranforma num array, que tem array dentro com chave.valor

Object.entries(pessoa).forEach(([chave, valor]) => {
  console.log(`${chave} -> ${valor}`); // call back com evento percorre Array
}); // desustructuring

// definindo nova propriedade no Obj
Object.defineProperty(pessoa, "dataNascimento", {
  enumerable: true,
  writable: false,
  value: `19/01/${2019 - pessoa.idade}`
});

// listando novamente, desustruturando o Array formado pelo 'entries' com callBack
// e o Evento que faz o callback é percorrer o array;
Object.entries(pessoa).forEach(([chave, valor]) => {
  console.log(`${chave} -> ${valor}`);
});

// Object.assign
const dest = { a: 1 };
const o1 = { b: 2 };
const o2 = { c: 3, a: 4 };
const obj = Object.assign(dest, o1, o2);
console.log(obj);
