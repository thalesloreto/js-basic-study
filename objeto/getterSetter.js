class Produto {
  constructor(nome = "Vazio", valor = 0) {
    this.nome = nome;
    let _valor = valor;
    this.setValor = valor => (_valor = valor);
    this.getValor = () => `Valor: R$${_valor}`;
  }
}

const p1 = new Produto("Mesa", 1683);
p1.setValor(9800);
console.log(p1.nome, p1.getValor());

const p2 = new Produto("Notebook", 2500);
console.log(p2.nome, p2.getValor());

// function Produto(nome = "Vazio", valor = 0) {
//   this.nome = nome;
//   let _valor = valor;
//   this.setValor = valor => (_valor = valor);
//   this.getValor = () => `Valor: R$${_valor}`;
// }
