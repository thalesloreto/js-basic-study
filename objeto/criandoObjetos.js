// 1 LITERAL
const obj1 = {};
console.log(obj1);

// 2
const obj2 = new Object();
console.log(obj2);

// 3 NORMAL INSTANCIANDO
function Produto(nome, preco, desconto) {
  this.nome = nome;
  this.getPrecoComDesconto = () => preco * (1 - desconto);
}

const p1 = new Produto("Caneta", 7.99, 0.15);
const p2 = new Produto("Notebook", 2998.99, 0.25);
console.log(p1.nome, p2.nome);
console.log(p1.getPrecoComDesconto(), p2.getPrecoComDesconto());

// 4 FACTORY
function criarFuncionario(nome, salarioBase, faltas) {
  return {
    nome,
    salarioBase,
    faltas,
    getSalarioMes() {
      return (salarioBase / 30) * (30 - faltas);
    }
  };
}

const f1 = criarFuncionario("Cindy", 19700, 0);
const f2 = criarFuncionario("Nhex", 11400, 2);
console.log(f1.getSalarioMes(), f2.getSalarioMes());

// 5 OBJECT.CREATE
const filha = Object.create(null);
filha.nome = "Ana";
console.log(filha);

// 6 JSON MODE
const fromJSON = JSON.parse('{"salve": "Sou um JSON"}');
console.log(fromJSON);
