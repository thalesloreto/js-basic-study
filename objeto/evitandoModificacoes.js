const produto = Object.preventExtensions({
  nome: "Qualquer", 
  preco: 1.99,
  tag: "Promoção"
})
console.log("Extensível:", Object.isExtensible(produto));

produto.nome = "Borracha";
produto.descricao = "Borracha escolar branca";
delete produto.tag;
console.log(produto);

//Object.seal
const pessoa = { nome: "Thales", idade: 19 };
Object.seal(pessoa);
console.log("Selado:", Object.isSealed(pessoa));

pessoa.sobrenome = "Loreto";
delete pessoa.nome;
pessoa.idade = 29;
console.log(pessoa);