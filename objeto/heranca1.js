const ferrari = {
  modelo: "V40",
  velMax: 320
};

const volvo = {
  modelo: "V33",
  velMax: 290
};

console.log(ferrari.__proto__);
console.log(ferrari.__proto__ === Object.prototype);
console.log(volvo.__proto__ === Object.prototype);

console.log(Object.prototype.__proto__);

function meuObjeto() {}
console.log(typeof Object, typeof meuObjeto);
console.log(Object.prototype, meuObjeto.prototype);
console.log(Object.prototype);
