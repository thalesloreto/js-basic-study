const pai = { nome: "Mauricio", corCabelo: "preto" };

const filha1 = Object.create(pai);
filha1.nome = "Ana";
console.log(filha1.nome, "tem cabelo", filha1.corCabelo);

const filha2 = Object.create(pai, {
  nome: { value: "Bia", writable: false, enumerable: true }
});
console.log(filha2);

console.log(Object.keys(filha1));
console.log(Object.keys(filha2));

for (let key in filha2) {
  if (filha2.hasOwnProperty(key)) {
    console.log(`${key} -> é dela mesmo!`);
  } else {
    console.log(`${key} -> NÃO É!`);
  }
}
