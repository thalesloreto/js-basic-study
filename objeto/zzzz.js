function funcao() {}

console.log(new funcao().__proto__ === funcao.prototype);
console.log(funcao.__proto__ === Function.prototype);
console.log(Function.prototype.__proto__ === Object.prototype); // final
