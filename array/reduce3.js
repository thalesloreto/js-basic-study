const alunos = [
  { nome: 'Thales', nota: 8.7, bolsista: true },
  { nome: 'Betinho', nota: 9.2, bolsista: false },
  { nome: 'Arthur', nota: 9.8, bolsista: false },
  { nome: 'Pedrinho', nota: 7.3, bolsista: true }
]

// Desafio 1: todos bolsista
const todosBolsistas = (inicial, final) => inicial && final
const resultado1 = alunos.map(a => a.bolsista).reduce(todosBolsistas)
console.log(resultado1)

// Desafio 2 : algum bolsista
const algumBolsista = (inicial, final) => inicial || final
const resultado2 = alunos.map(a => a.bolsista).reduce(algumBolsista)
console.log(resultado2)
