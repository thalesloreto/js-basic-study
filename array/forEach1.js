const aprovados = ["Julia", "Natalia", "Pedro", "Felipe"];

/*
  For Each recebe 2 parametros
  (1-> conteúdo do array, 2-> índice do elemento)
*/

aprovados.forEach((nome, indice) => {
  console.log(`${indice}-> ${nome}`);
});

aprovados.forEach(nome => console.log(nome));
console.log("-------------------------------------------------");

const exibeAprovados = aprovados => console.log(aprovados);
aprovados.forEach(exibeAprovados);
