// eslint-disable-next-line no-extend-native
Array.prototype.filter2 = function (callback) {
  const newArray = []
  for (let i = 0; i < this.length; i++) {
    if (callback(this[i], i, this)) {
      newArray.push(this[i])
    }
  }
  return newArray
}

const produtos = [
  { nome: 'Notebook', preco: 2499, fragil: true },
  { nome: 'iPad Pro', preco: 4199, fragil: true },
  { nome: 'Copo de Vidro', preco: 12.55, fragil: true },
  { nome: 'Copo de Plástico', preco: 15.9, fragil: false }
]

const caroFragil = produto => produto.preco >= 2500 && produto.fragil
console.log(produtos.filter2(caroFragil))
