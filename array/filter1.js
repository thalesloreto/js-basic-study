const produtos = [
  { nome: 'Notebook', preco: 2499, fragil: true },
  { nome: 'iPad Pro', preco: 4199, fragil: true },
  { nome: 'Copo de Vidro', preco: 12.55, fragil: true },
  { nome: 'Copo de Plástico', preco: 15.9, fragil: false }
]

// const caroFragil = produto => produto.preco >= 500 && produto.fragil; //melhor
const caro = produto => produto.preco >= 500
const fragil = produto => produto.fragil

console.log(produtos.filter(caro).filter(fragil))
