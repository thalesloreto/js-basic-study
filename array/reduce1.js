const alunos = [
  { nome: 'Thales', nota: 8.7, bolsista: true },
  { nome: 'Betinho', nota: 9.2, bolsista: false },
  { nome: 'Arthur', nota: 9.8, bolsista: false },
  { nome: 'Pedrinho', nota: 7.3, bolsista: true }
]
console.log(alunos.map(al => al.nota))

const resultado = alunos
  .map(al => al.nota)
  .reduce(function (acumulador, valorAtual) {
    console.log(acumulador, valorAtual)
    return acumulador + valorAtual
  })

console.log(resultado)
