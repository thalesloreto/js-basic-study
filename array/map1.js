const nums = [1, 2, 3, 4, 5]

let resultado = nums.map(elemento => elemento * 2)
console.log(resultado)

const soma10 = x => x + 10
const triplo = x => x * 3
const paraDinheiro = x =>
  `R$${parseFloat(x)
    .toFixed(2)
    .replace('.', ',')}`

resultado = nums
  .map(soma10)
  .map(triplo)
  .map(paraDinheiro)
console.log(resultado)
