const pilotos = ["Piloto0", "Piloto1", "Piloto2", "Piloto3"];
pilotos.pop();
console.log(pilotos);

pilotos.push("Piloto4");
console.log(pilotos);

pilotos.shift();
console.log(pilotos);

pilotos.unshift("Piloto5");
console.log(pilotos);

//add splice
pilotos.splice(2, 0, "Piloto6", "Piloto7");
console.log(pilotos);

//remove splice
pilotos.splice(3, 1);
console.log(pilotos);

//SLICE retorna novo array uma fatia
const algunsPilotos1 = pilotos.slice(2); // retorna novo array
console.log(algunsPilotos1);

const algunsPilotos2 = pilotos.slice(1, 4);
console.log(algunsPilotos2);
