const alunos = [
  { nome: 'Thales', nota: 8.7, bolsista: true },
  { nome: 'Betinho', nota: 9.2, bolsista: false },
  { nome: 'Arthur', nota: 9.8, bolsista: false },
  { nome: 'Pedrinho', nota: 7.3, bolsista: true }
]

// Desafio 1: Todos os alunos são bolsista?
const resultado1 = alunos.reduce(function (acumulador, alunoAtual) {
  if (!alunoAtual.bolsista) {
    acumulador = false
  }
  return acumulador
}, true)

console.log(resultado1)

const resultado2 = alunos.reduce((acumulador, alunoAtual) => {
  if (alunoAtual.bolsista) {
    return true
  }
  return acumulador
}, false)

console.log(resultado2)
