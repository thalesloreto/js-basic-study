import React, { Component } from "react";

export default class Saudacao extends Component {
  state = {
    nome: this.props.nome,
    sobrenome: this.props.sobrenome
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    const { nome, sobrenome } = this.state;
    return (
      <div>
        <h1>
          {nome} {sobrenome}!{" "}
        </h1>
        <hr />

        <input
          type="text"
          placeholder="Nome"
          name="nome"
          value={nome}
          onChange={this.onChange}
        />

        <input
          type="text"
          placeholder="Sobrenome"
          name="sobrenome"
          value={sobrenome}
          onChange={this.onChange}
        />
      </div>
    );
  }
}
