import React from "react";
import ReactDOM from "react-dom";

import Saudacao from "./components/Saudacao";

ReactDOM.render(
  <Saudacao nome="Thales" sobrenome="Loreto" />,
  document.getElementById("root")
);
