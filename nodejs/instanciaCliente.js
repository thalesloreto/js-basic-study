const contadorA = require('./instanciaUnica') // faz cache, retorna a mesma
const contadorB = require('./instanciaUnica') // instancia

const contadorC = require('./instanciaNova')() // factory, extraindo obj
const contadorD = require('./instanciaNova')() // novas instancias

contadorA.inc()
contadorA.inc()
console.log('A:', contadorA.valor, 'B:', contadorB.valor)

contadorC.inc()
contadorC.inc()
console.log('C:', contadorC.valor, 'D:', contadorD.valor)
