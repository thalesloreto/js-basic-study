const url = 'http://files.cod3r.com.br/curso-js/funcionarios.json'
const axios = require('axios')

const pegaChineses = funcionario => funcionario.pais === 'China'
const pegaMulher = funcionario => funcionario.genero === 'F'
// const pegaSalario = funcionario => funcionario.salario MAP Salário
const menorSalario = (inicial, final) => {
  return inicial.salario < final.salario ? inicial : final
}

axios.get(url).then(response => {
  const funcionarios = response.data

  // Mulher chinesa com menor salário
  const chinesas = funcionarios
    .filter(pegaChineses)
    .filter(pegaMulher)
    .reduce(menorSalario)

  console.log(chinesas)
})
