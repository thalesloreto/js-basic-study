function falaDepoisDe (segundos, frase) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(frase)
    }, segundos * 1000)
  })
}

falaDepoisDe(5, 'Salve')
  .then(frase => frase.concat('!!!'))
  .then(frase => console.log(frase))
  .catch(err => console.log(err)) // tratar erro

console.log('Depois da promise')
