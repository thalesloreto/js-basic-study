/// ////////////////////////////////////
const palavra = 'Salve'
for (let letra of palavra) {
  console.log(letra)
}

const nomes = ['Thales', 'Cindy', 'Marcio']

for (let nome of nomes) {
  console.log(nome)
}

/// ///////////////////////////////////

const assuntosMap = new Map([
  ['Map', { abordado: true }],
  ['Set', { abordado: true }],
  ['Promise', { abordado: false }]
])

for (let assuntos of assuntosMap) {
  console.log(assuntos)
}

for (let chaves of assuntosMap.keys()) {
  console.log(chaves)
}

for (let valores of assuntosMap.values()) {
  console.log(valores)
}

for (let [key, value] of assuntosMap.entries()) {
  console.log(key, value)
}
