/**
 * Operador '...' rest / spread
 * Usar rest com parâmetro de função
 *
 * usar spread com objeto
 */
const funcionario = { nome: 'Thales', salario: 12999.5 }
const clone = { ...funcionario, nome: 'Cindy' }
console.log(clone)

// usar spread com array
const arr = ['João', 'Pedro', 'Nicolas']
const finalArr = ['Lucas', ...arr, 'Thales']
console.log(finalArr)
