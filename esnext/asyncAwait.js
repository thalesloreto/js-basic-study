// com promise

const http = require('http')

const getTurma = letra => {
  const url = `http://files.cod3r.com.br/curso-js/turma${letra}.json`

  return new Promise((resolve, reject) => {
    http.get(url, res => {
      let resultado = ''

      res.on('data', dados => {
        resultado += dados
      })

      res.on('end', () => {
        try {
          resolve(JSON.parse(resultado))
        } catch (err) {
          reject(err)
        }
      })
    })
  })
}

// Async Await
// facilitar promisses
const obterTurmas = async () => {
  const turmaA = await getTurma('A')
  const turmaB = await getTurma('B')
  const turmaC = await getTurma('C')

  return [].concat(turmaA, turmaB, turmaC)
}

obterTurmas()
  .then(turmas => turmas.map(alunos => alunos.nome))
  .then(alunos => console.log(alunos))
