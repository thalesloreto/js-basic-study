// Arrow Function
const soma = (a, b) => a + b
console.log(soma(2, 3))

// Arrow Function (this)
const lexico01 = () => console.log(this === exports)
lexico01()

// Parametro Default
const log = (texto = 'Node') => {
  return console.log(texto)
}

log()
log('Salve Família')

// Operador Rest
function total (...numeros) {
  return numeros.reduce((total, next) => total + next)
}
console.log(total(5))
