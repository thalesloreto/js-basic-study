// Par  Nome  ||  Valor
const saudacao = 'Salve';

function exec() {
    const saudacao = 'Falaaa';
    return saudacao;
}

const cliente = {
    nome: 'Thales',
    idade: 19,
    peso: 62,
    endereco: {
        rua: 'Rua São Manoel',
        numero: 38
    }
}

console.log(saudacao);
console.log(exec());
console.log(cliente);