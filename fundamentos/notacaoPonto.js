console.log(Math.ceil(6.1)); // Math = object .ceil = funcao dentro do objeto

const obj1 = {};
obj1.nome = 'Bola';
console.log(obj1.nome);

function Objeto(nome) {
    this.nome = nome;
    this.exec = function() {
        console.log('Executando ...');
    }
}

// console.log(typeof Objeto);
const obj2 = new Objeto('Cadeira');
const obj3 = new Objeto('Mesa');
console.log(obj2.nome);
console.log(obj3.nome);
obj3.exec();