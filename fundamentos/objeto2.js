console.log(typeof Object); // func
console.log(typeof new Object); // obj;

const Cliente = function() {}
console.log(typeof Cliente); // func
console.log(typeof new Cliente); // obj

class Produto {};
console.log(typeof Produto); // func
console.log(typeof new Produto()); // obj