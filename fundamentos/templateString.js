const nome = 'Thales';
const concatenacao = 'Olá ' + nome + ' !';

console.log(concatenacao);

// Templates

const template = `Olá ${nome}! \n`;
console.log(template);

console.log(`1 + 1 = ${1 + 1}`);

const up = texto => texto.toUpperCase();

console.log(`Ei... ${up('cuidado')} !!`);