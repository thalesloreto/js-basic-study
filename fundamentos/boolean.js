let isAtivo = false;
console.log(isAtivo);

isAtivo = true;
console.log(isAtivo);

isAtivo = 1;
console.log(!!isAtivo);


// Verdadeiros

console.log('Os verdadeiros: ');
console.log(!!3);
console.log(!!' '); //espaço vazio
console.log(!!'texto texto');
console.log(!![]);
console.log(!!{});
console.log(!!Infinity);
console.log(!!(isAtivo = true)); // conta apenas o valor.

// Falsos

console.log('Os falsos: ');
console.log(!!0);
console.log(!!'');
console.log(!!null);
console.log(!!undefined);
console.log(!!NaN);
console.log(!!(isAtivo = false));

// --*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*

console.log('Para finalizar...');
console.log(!!('' || null || 0 || 'eusouverdadeiro'));

let nome = '';
console.log(nome || 'Desconhecido');