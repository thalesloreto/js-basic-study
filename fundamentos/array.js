const valores = [7.9, 6.3, 9.4, 8.7];
console.log(valores[0], valores[3]);
console.log(valores[4]);

valores[4] = 10.2;
console.log(valores);
console.log(valores.length);

valores.push({id: 3}, false, null, 'teste');
console.log(valores);

console.log(valores.pop()); // retira o ultimo elemento
delete valores[0];
console.log(valores);

console.log(typeof valores);