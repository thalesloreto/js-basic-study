function tratarErroELancar(erro) {
    // throw new Error('...');
    throw {
        nome: erro.name,
        msg: erro.message,
        date: new Date
    }
}

function imprimirNomeGritado( { name } ) {
    try {
        console.log(name.toUpperCase() + '!!');
    } catch (e) {
        tratarErroELancar(e);
    }
}

const obj = {
    nome: 'Thales'
}
imprimirNomeGritado(obj);