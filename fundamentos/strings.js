const escola = 'Cod3r';

console.log(escola.charAt(4));
console.log(escola.charAt(0));

console.log(escola.charCodeAt(3)); // código html do indice 3 ('3')
console.log(escola.indexOf('d'));

console.log(escola.substring(1));
console.log(escola.substring(0, 3));

console.log('Escola '.concat(escola).concat('!'));
console.log(escola.replace('C', 'e'));

console.log('Ana,Maria,Pedro'.split(','));