// Armazenando função em uma variável
const printSum = function (a = 0, b = 0) {
    console.log(a + b);
}

printSum(2, 3);

// Armazenando em função Arrow
const sum = (a = 0, b = 0) => {
    return a + b;
}

console.log(sum(2, 3));

// retorno implícito
const sub = (a = 0, b = 0) => a - b;
console.log(sub(2, 3));