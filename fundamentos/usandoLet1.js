var numero = 1; // escopo global -> let também funciona
{
    let numero = 2;
    console.log('Dentro =', numero); // preferencia escopo menor
}
console.log('Fora =', numero);