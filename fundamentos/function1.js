function printSum(a, b) {
    console.log(a + b);
}

printSum(2, 3);
printSum(2);

function sumReturn(a, b = 0) {
    return a + b;
}

console.log(sumReturn(2, 3));
console.log(sumReturn(2));
console.log(sumReturn());