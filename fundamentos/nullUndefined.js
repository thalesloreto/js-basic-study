let valor;
console.log(valor); // valor não declarado 'undefined'

valor = null; // ausência de valor 'nulo'
console.log(valor);

const produto = {};
console.log(produto.preco);
console.log(produto);

produto.preco = 3.50;
console.log(produto);

produto.preco = undefined; // evite atribuir undefined.
console.log(!!produto.preco);
console.log(produto);

produto.preco = null; // o mais recomendado.
console.log(!!produto.preco);
console.log(produto);

delete produto.preco; // exclui totalmente o preco do produto
console.log(produto);
console.log(produto.preco);