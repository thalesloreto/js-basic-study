
function compras(trabalho1, trabalho2){
    const comprarSorvete = trabalho1 || trabalho2;
    const tvCinqDuas = trabalho1 && trabalho2;
    const tvTrinDuas = trabalho1 != trabalho2;
    const manterSaudavel = !comprarSorvete;

    return { comprarSorvete, tvCinqDuas, tvTrinDuas, manterSaudavel };
}

console.log(compras(true, true));