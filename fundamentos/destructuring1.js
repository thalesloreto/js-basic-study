// ES6
const pessoa = {
    nome: 'Thales',
    idade: 19,
    endereco: {
        rua: 'Rua ABD',
        numero: 103
    },
    peso: 62.6
}

const { nome, idade } = pessoa;
console.log(nome, idade);

const { nome: n, idade: i} = pessoa;
console.log(n, i);

//sem existir
const { sobrenome, bemHumorada = true } = pessoa;
console.log(sobrenome, bemHumorada);

const { endereco: { rua, numero, cep } } = pessoa;
console.log(rua, numero, cep);